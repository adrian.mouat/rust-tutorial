use std::collections::HashSet;
use std::collections::HashMap;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    let input = read_to_string("day3_in.txt").expect("Unable to read input");
    let p: Vec<&str> = input.lines().collect();
    
    let res = intersection_point(p[0], p[1]);

    println!("Answer = {}", res);
    Ok(())
}

fn intersection_point(c1: &str, c2: &str) -> isize {
    let mut pt_1 = (0, 0);
    let mut c1_visited: HashMap<(isize, isize), isize> = HashMap::new();

    let mut steps = 0;
    for vector in c1.split(',') {
        let (dir, mag) = vector.split_at(1);
        let mag = mag.parse::<isize>().unwrap();
        
        match dir {
            "R" => {

                for pt_i in 1..=mag {
                    steps += 1;
                    c1_visited.insert((pt_1.0 + pt_i, pt_1.1), steps);
                };
                pt_1.0 += mag;
            }
            "L" => {

                for pt_i in 1..=mag {
                    steps += 1;
                    c1_visited.insert((pt_1.0 - pt_i, pt_1.1), steps);
                };
                pt_1.0 -= mag;
            }
            "U" => {

                for pt_i in 1..=mag {
                    steps += 1;
                    c1_visited.insert((pt_1.0, pt_1.1 + pt_i), steps);
                };
                pt_1.1 += mag;
            }
            "D" => {

                for pt_i in 1..=mag {
                    steps += 1;
                    c1_visited.insert((pt_1.0, pt_1.1 - pt_i), steps);
                };
                pt_1.1 -= mag;
            }
            _ => panic!("Shouldn't happen!"),
        }
    };
    
    let mut pt_2 = (0, 0);
    let mut c2_visited: HashMap<(isize, isize), isize> = HashMap::new();
    steps = 0;
    for vector in c2.split(',') {
        let (dir, mag) = vector.split_at(1);
        let mag = mag.parse::<isize>().unwrap();
        
        match dir {
            "R" => {

                for pt_i in 1..=mag {
                    steps += 1;
                    c2_visited.insert((pt_2.0 + pt_i, pt_2.1), steps);
                };
                pt_2.0 += mag;
            }
            "L" => {

                for pt_i in 1..=mag {
                    steps += 1;
                    c2_visited.insert((pt_2.0 - pt_i, pt_2.1), steps);
                };
                pt_2.0 -= mag;
            }
            "U" => {

                for pt_i in 1..=mag {
                    steps += 1;
                    c2_visited.insert((pt_2.0, pt_2.1 + pt_i), steps);
                };
                pt_2.1 += mag;
            }
            "D" => {
                for pt_i in 1..=mag {
                    steps += 1;
                    c2_visited.insert((pt_2.0, pt_2.1 - pt_i), steps);
                };
                pt_2.1 -= mag;
            }
            _ => panic!("Shouldn't happen!"),
        }
    };

    c1_visited.remove(&(0,0));
    c2_visited.remove(&(0,0));
    let c1_visited_set = c1_visited.keys().fold(HashSet::new(), |mut hs, k| {
        hs.insert(k);
        hs
    });
    let c2_visited_set = c2_visited.keys().fold(HashSet::new(), |mut hs, k| {
        hs.insert(k);
        hs
    });
    let intersections: Vec<&&(isize, isize)> = c1_visited_set.intersection(&c2_visited_set).collect();
 
    intersections
        .iter()
        .map(|intersection| c1_visited.get(intersection).unwrap() + c2_visited.get(intersection).unwrap())
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests {
    use super::intersection_point;

    #[test]
    fn test_intersections() {
        assert_eq!(intersection_point("R8,U5,L5,D3", "U7,R6,D4,L4"), 30);
        assert_eq!(
            intersection_point(
                "R75,D30,R83,U83,L12,D49,R71,U7,L72",
                "U62,R66,U55,R34,D71,R55,D58,R83"
            ),
            610
        );
        assert_eq!(
            intersection_point(
                "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
                "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"
            ),
            410
        );
    }
}
