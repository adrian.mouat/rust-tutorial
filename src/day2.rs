use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    
    let input = read_to_string("day2_in.txt").expect("Unable to read input");
    let mut p: Vec<isize> = input.split(',').map(|item| item.trim().parse::<isize>().unwrap()).collect();

    let orig = p.clone();
    p[1] = 12;
    p[2] = 2;
    let out = run_program(p);
    
    println!("{:?}", out);

    for i in 0..100 {
        for j in 0..100 {
            let mut t = orig.clone();
            t[1] = i;
            t[2] = j;
            let out = run_program(t);
            if out[0] == 19690720 {
                println!("Pair is {} {}", i, j);
                let answer = 100 * i + j;
                println!("Answer is {}", answer);
            }
        }
    }
    Ok(())
}

fn run_program(mut p: Vec<isize>) -> Vec<isize> {

    let mut i = 0;
    loop {
        match p[i] {
            99 => {return p;}
            1 => {
                let dest = p[i+3] as usize;
                let a = p[i+1] as usize;
                let b = p[i+2] as usize;
                p[dest] = p[a] + p[b]; 
            }
            2 => {
                let dest = p[i+3] as usize;
                let a = p[i+1] as usize;
                let b = p[i+2] as usize;
                p[dest] = p[a] * p[b];
            }
            _ => { }
        };
        i += 4;

    }
}

#[cfg(test)]
mod tests {
    use super::run_program;

    #[test]
    fn test_computer() {
        assert_eq!(run_program(vec![1,0,0,0,99]), [2,0,0,0,99]);
        assert_eq!(run_program(vec![2,3,0,3,99]), [2,3,0,6,99]);
        assert_eq!(run_program(vec![2,4,4,5,99,0]), [2,4,4,5,99,9801]);
        assert_eq!(run_program(vec![1,1,1,4,99,5,6,0,99]), [30,1,1,4,2,5,6,0,99]);
    }
}
