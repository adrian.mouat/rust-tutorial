use std::collections::HashSet;
use std::collections::HashMap;
use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {

    let answer = (128392..643281).filter(|n| valid_code(*n)).count();
    println!("{}", answer);
    valid_code(111111);
    Ok(())
}

fn valid_code(code: usize) -> bool {
    let code = format!("{}", code);
    let code = code.chars(); 

    let mut run = 1;
    let mut have_double = false;

    let mut p: u32 = 0;
    let mut size: u32 = 0;
    for c in code {
        size+=1;

        let c = u32::from_str_radix(String::from(c).as_str(), 10).unwrap();

        if c == p {
            run+=1;
        } else {
            if run == 2 {
                have_double = true;
            }
            run = 1;

        }    

        if  c < p {
            return false;
        }

        p = c;
    }

    (run == 2 || have_double) && (size == 6)
}

#[cfg(test)]
mod tests {

    use super::valid_code;
    #[test]
    fn test_codes() {
        assert!(valid_code(111123));
        assert!(!valid_code(135679));
        assert!(valid_code(111111));
        assert!(!valid_code(223450));
        assert!(!valid_code(123789));
        assert!(!valid_code(55555));
    }
}
