use std::{error::Error, collections::HashMap};
use std::fs::read_to_string;



fn main() -> Result<(), Box<dyn Error>> {
    
    let input = read_to_string("day6_in.txt").expect("Unable to read input");

    let ans = total_orbits(&input);

    println!("Total orbits: {}", ans);

    let transfers = total_transfers(&input);

    println!("Total transfers: {}", transfers);

    Ok(())
}

fn get_num_orbits(object: &str, orbits: &HashMap<String, String>) -> usize {

    if orbits.get(object).unwrap() == "COM" {
        return 1;
    } else {
        return 1 + get_num_orbits(&orbits.get(object).unwrap(), orbits);
    }
}

fn get_path(object: &str, orbits: &HashMap<String, String>) -> Vec<String> {

    let next = orbits.get(object).unwrap();
    if next == "COM" {
        let mut ret = Vec::new();
        ret.push("COM".to_string());
        return ret;
    } else {
        let mut ret = get_path(&next, orbits);
        ret.push(next.clone());
        return ret;
    }
}

fn build_orbits(input: &str) -> HashMap<String, String> {
    let mut orbits: HashMap<String, String> = HashMap::new();
    for line in input.lines() {
        let v: Vec<&str> = line.split(')').collect();
        let a = v.get(0).unwrap();
        let b = v.get(1).unwrap();
        println!("a {} b {}", a ,b);
        orbits.insert(b.to_string(), a.to_string());
    }
    orbits
}

fn total_orbits(input: &str) -> usize {

    let orbits = build_orbits(input);
    println!("orbits {:?}", orbits);
    let mut total_orbits = 0;
    for key in orbits.keys() {
        total_orbits += get_num_orbits(key, &orbits);
    }
    total_orbits
}

fn find_last_common(a: &Vec<String>, b: &Vec<String>) -> String {

    let mut my_b = b.clone();
    my_b.reverse();
    for t in my_b {
        if a.contains(&t) {
            return t;
        }
    }
    "".to_string()
}
fn total_transfers(input: &str) -> usize {

    let orbits = build_orbits(input);

    let my_path = get_path("YOU", &orbits);
    let santa_path = get_path("SAN", &orbits);

    println!("mypath {:?} santa_path{:?}", my_path, santa_path);

    let common = find_last_common(&my_path, &santa_path);

    println!("common {}", common);
    let len1 = &my_path.len() - my_path.iter().position(|x| x == &common).unwrap();
    let len2 = &santa_path.len() - santa_path.iter().position(|x| x == &common).unwrap();

    return len1 + len2 - 2;
}

#[cfg(test)]
mod tests {
    use super::total_orbits;
    use super::total_transfers;

    #[test]
    fn test_orbits() {
        let input = "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L";
        assert_eq!(total_orbits(input), 42);

    }

    #[test]
    fn test_transfers() {

        let input = "COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN";

        assert_eq!(total_transfers(input), 4);
    }
}
