use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    
    let input = read_to_string("day5_in.txt").expect("Unable to read input");
    let mut p: Vec<isize> = input.split(',').map(|item| item.trim().parse::<isize>().unwrap()).collect();

    let out = run_program(&mut p, 5);
    
    println!("Output: {}", out);

    Ok(())
}

fn run_program(p: &mut Vec<isize>, input: isize) -> isize {

    let mut i = 0;
    let mut output = 0;
    loop {
        let mut mode_op = format!("{}", p[i]);
        //println!("Running {} ins {}", mode_op, i);
        let last = mode_op.pop().unwrap();
        let pen = mode_op.pop().unwrap_or('0');
        let mode_1 = mode_op.pop().unwrap_or('0');
        let mode_2 = mode_op.pop().unwrap_or('0');
        let mode_3 = mode_op.pop().unwrap_or('0');
        
        let op: isize = format!("{}{}", pen, last).parse().unwrap();

        match op {
            99 => {
                println!("output = {}", output);
                return output;
            }
            1 => {
                let dest = p[i+3] as usize;
                
                let a = if mode_1 == '1' {
                    p[i+1]
                } else {
                     p[p[i+1] as usize]
                };

                let b = if mode_2 == '1' {
                    p[i+2] 
                } else {
                    p[p[i+2] as usize]
                };
                
                p[dest] = a + b; 
                i += 4;
            }
            2 => {
                let dest = p[i+3] as usize;
                
                let a = if mode_1 == '1' {
                    p[i+1]
                } else {
                     p[p[i+1] as usize]
                };

                let b = if mode_2 == '1' {
                    p[i+2] 
                } else {
                    p[p[i+2] as usize]
                };

                p[dest] = a * b;
                i += 4;
            }
            3 => {
                let index = p[i+1] as usize;
                p[index] = input;
                i += 2;
            }
            4 => {
                output = if mode_1 == '1' {
                    p[i+1]
                } else {
                    p[p[i+1] as usize]
                };

                println!("output = {}", output);
                i += 2;
            }
            5 => {

                let a = if mode_1 == '1' {
                    p[i+1]
                } else {
                     p[p[i+1] as usize]
                };

                let pos = if mode_2 == '1' {
                    p[i+2] 
                } else {
                    p[p[i+2] as usize]
                };

                if a != 0 {
                    i = pos as usize; 
                } else {
                    i +=3;
                }
            }
            6 => {

                let a = if mode_1 == '1' {
                    p[i+1]
                } else {
                     p[p[i+1] as usize]
                };

                let pos = if mode_2 == '1' {
                    p[i+2] 
                } else {
                    p[p[i+2] as usize]
                };

                if a == 0 {
                    i = pos as usize; 
                } else {
                    i +=3;
                }
            }
            7 => {
                let dest = p[i+3] as usize;
                
                let a = if mode_1 == '1' {
                    p[i+1]
                } else {
                     p[p[i+1] as usize]
                };

                let b = if mode_2 == '1' {
                    p[i+2] 
                } else {
                    p[p[i+2] as usize]
                };
                
                if a < b {
                    p[dest] = 1;
                } else {
                    p[dest] = 0;
                }
                i += 4;

            }

            8 => {
                let dest = p[i+3] as usize;
                
                let a = if mode_1 == '1' {
                    p[i+1]
                } else {
                     p[p[i+1] as usize]
                };

                let b = if mode_2 == '1' {
                    p[i+2] 
                } else {
                    p[p[i+2] as usize]
                };
                
                if a == b {
                    p[dest] = 1;
                } else {
                    p[dest] = 0;
                }
                i += 4;

            }
            _ => { panic!("Got impossible code {}", mode_op); }
        };
        

    }
}

#[cfg(test)]
mod tests {
    use super::run_program;

    #[test]
    fn test_computer() {
        let mut p = vec![1,0,0,0,99];
        run_program(&mut p, 0);
        assert_eq!(p, [2,0,0,0,99]);
      
        p = vec![2,3,0,3,99];
        run_program(&mut p, 0);
        assert_eq!(p, [2,3,0,6,99]);

        p = vec![2,4,4,5,99,0];
        run_program(&mut p, 0);
        assert_eq!(p,[2,4,4,5,99,9801]);

        p = vec![1,1,1,4,99,5,6,0,99];
        run_program(&mut p, 0);
        assert_eq!(p,[30,1,1,4,2,5,6,0,99]);
      
        p = vec![1002,4,3,4,33];
        run_program(&mut p, 0);
        assert_eq!(p,[1002,4,3,4,99]);

        p = vec![1002,4,3,4,33];
        run_program(&mut p, 0);
        assert_eq!(p,[1002,4,3,4,99]);
      
        assert_eq!(run_program(&mut vec![3,9,8,9,10,9,4,9,99,-1,8], 8), 1);
        assert_eq!(run_program(&mut vec![3,9,7,9,10,9,4,9,99,-1,8], 8), 0);
        assert_eq!(run_program(&mut vec![3,3,1108,-1,8,3,4,3,99], 8), 1);
        assert_eq!(run_program(&mut vec![3,3,1107,-1,8,3,4,3,99], 8), 0);
        
        assert_eq!(run_program(&mut vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 1), 1);
        assert_eq!(run_program(&mut vec![3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9], 0), 0);

        assert_eq!(run_program(&mut vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 111), 1);
        assert_eq!(run_program(&mut vec![3,3,1105,-1,9,1101,0,0,12,4,12,99,1], 0), 0);
        
        assert_eq!(run_program(&mut vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,
            31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,
            46,1101,1000,1,20,4,20,1105,1,46,98,99], 7), 999);

        //assert_eq!(run_program(&mut vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,
        //    31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,
        //    46,1101,1000,1,20,4,20,1105,1,46,98,99], 8), 1000);

        //assert_eq!(run_program(&mut vec![3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,
        //    31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,
        //    46,1101,1000,1,20,4,20,1105,1,46,98,99], 25), 1001);
    

    }
}
