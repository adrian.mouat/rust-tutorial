use std::error::Error;
use std::fs::read_to_string;

fn main() -> Result<(), Box<dyn Error>> {
    
    let input = read_to_string("input.txt").expect("Unable to read input");
    let total_fuel: usize = input.lines().map(|line| line.parse::<usize>()).map(|i| i.unwrap_or(0)).map(find_fuel_for_fuel).sum();

    println!("{:?}", total_fuel);

    Ok(())
}

fn calculate_fuel(mass: usize) -> usize {

    let fuel: isize = (mass as isize / 3) - 2;
    if fuel > 0{
        fuel as usize
    } else {
        0
    }
}

fn find_fuel_for_fuel(mass: usize) -> usize {

    let mut f = calculate_fuel(mass);
    let mut total = f;
    while f > 0 {
        f = calculate_fuel(f);
        total = total + f;
    }
    total
}

#[cfg(test)]
mod tests {
    use super::calculate_fuel;

    #[test]
    fn testfuel() {
        assert_eq!(calculate_fuel(12), 2);
        assert_eq!(calculate_fuel(1969), 654);
        assert_eq!(calculate_fuel(100756), 33583);
    }
}